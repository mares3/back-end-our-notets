
# Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
# Click nbfs://nbhost/SystemFileSystem/Templates/Other/Dockerfile to edit this template

FROM openjdk:11
EXPOSE 8080
ADD target/back-end-our-notes.jar back-end-our-notes.jar
ENTRYPOINT ["java","-jar","/back-end-our-notes.jar"]
